from os import system
from paste_check.checker import *
from auxiliary.color_utils import *
from colorama import Fore, Style


def print_brand():
    print(f"{Fore.YELLOW}Da{Fore.MAGENTA}zz{Fore.CYAN}le{Fore.WHITE} Story Archive Script.{Style.RESET_ALL}")


if __name__ == '__main__':
    current_paste_link = "http://pastebin.com/NjS5LgYv"
    exit_loop = False
    while exit_loop is False:
        system("clear")
        print_brand()
        print_info(f"The link to check is set to {current_paste_link}")
        choice = input("[0]: Continue(default)\n[1]: Change Link\n[2]: Quit\n--Choice: ")
        if choice == '2':
            print_general("Roger that, admiral.")
            quit(0)
        if choice == '1':
            print_warning("Warning: The paste must be of the format 'http(s)://pastebin.com/blahblah'")
            new_link = input("New Link: ")
            current_paste_link = new_link
        else:
            print_general(f"Ok - will use {current_paste_link} as the link.\n")
            exit_loop = True
    try:
        scrape_result = scrape_primary_paste(current_paste_link)
        print_general("Operation was a success.")

        # Unpack the results
        alive_users = scrape_result['users'][0]
        dead_users = scrape_result['users'][1]
        alive_pastes = scrape_result['pastes'][0]
        dead_pastes = scrape_result['pastes'][1]
        misc_results = scrape_result['misc']

        total_users = len(alive_users) + len(dead_users)
        total_pastes = len(alive_pastes) + len(dead_pastes)
        print("----------------------------------------------------------------------")
        print_info(f"{len(dead_users)} / {total_users} users are dead :(")
        print_info(f"{len(dead_pastes)} / {total_pastes} pastes are gone :(")
        print("----------------------------------------------------------------------")

        print_general("Dumping all results to output file...")
        with open("story_archive.txt", 'w+') as output:
            output.write("=----= Stories =----=\n")
            for alive_paste in alive_pastes:
                output.write(f"\n     {alive_paste.title} by {alive_paste.author}\n")
                output.write(f"         {alive_paste.link}\n")
            output.write("\n=----= Dead Stories =----=\n")
            for dead_paste in dead_pastes:
                output.write(f"\n [!] Dead Paste - {dead_paste.link}")
            output.write("\n\n\n\n\n\n")
            output.write("=----= Writers =----=\n")
            for alive_user in alive_users:
                output.write(f"{alive_user.username} -- {alive_user.link}\n")
            for dead_user in dead_users:
                output.write(f"\n [!] Dead User - {dead_user.link}")
            output.write("\n\n\n\n\n\n")
            output.write("=----= Misc =----=\n")
            for misc in misc_results:
                output.write(f"\n     {misc}")
        print_general("File dumped!")
    except Exception as ex:
        print_error(ex)
        exit(1)
