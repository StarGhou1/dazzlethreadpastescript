import bs4
import requests
from auxiliary.color_utils import *
from paste_check.paste import Paste
from paste_check.user import User


def is_paste_dead(request):
    return request.status_code == 404


def is_user_dead(request):
    return request.status_code == 302  # lol


def scrape_paste(paste_link):
    paste = Paste(link=paste_link)
    paste_request = requests.get(paste_link)
    if is_paste_dead(paste_request):
        paste.dead = True
        return paste
    paste_parsed = bs4.BeautifulSoup(paste_request.content, features="html.parser")
    paste.title = paste_parsed.find("div", "paste_box_line1").get("title")
    try:
        paste.author = paste_parsed.find("div", "paste_box_line2").find("a").contents[0]
    except AttributeError:  # Occurs if a link to the author doesn't exist, in which case, it's a guest paste.
        paste.author = "Anonymous"

    return paste


def scrape_user(user_link):
    user = User(link=user_link)
    user_request = requests.get(user_link)
    if is_user_dead(user_request):
        user.dead = True
        return user
    user_parsed = bs4.BeautifulSoup(user_request.content, features="html.parser")
    user.username = user_parsed.find("div", "paste_box_line_u1").find("h1").contents[0].split("'")[0]
    return user


def scrape_primary_paste(primary_paste_url):
    final_result = {}  # { users : users_list[], pastes : pastes_list[], misc : misc_list[] }
    pastes_list = []  # pastes : [ alive_pastes[], dead_pastes[] ]
    users_list= []  # users : [ alive_users[], dead_users[] ]
    alive_pastes = []
    dead_pastes = []
    alive_users = []
    dead_users = []
    misc_list = []

    print_info("Scraping and parsing the primary paste...")

    if "pastebin.com/" not in primary_paste_url:
        raise Exception("Error: This is not a valid pastebin url.")
    primary_paste_id = primary_paste_url.split('/')[3]

    primary_paste_raw_url = f"https://pastebin.com/raw/{primary_paste_id}"
    primary_paste = requests.get(primary_paste_raw_url)

    if is_paste_dead(primary_paste):
        raise Exception("Error: The primary paste does not exist.")

    primary_paste_parsed = bs4.BeautifulSoup(primary_paste.content, features="html.parser")
    primary_paste_lines = primary_paste_parsed.prettify().split('\n')

    print_info("Paste has been scraped and parsed; beginning link harvest...")

    links = []
    link_filter = [".com", ".net", ".org"]
    for line in primary_paste_lines:
        if any(c in line for c in link_filter):
            line_split = line.split('//')
            if len(line_split) == 2:
                link = line_split[1].split(' ')[0].split('\r')[0]
                full_link = f"https://{link}"
                links.append(full_link)
    link_amount = len(links)
    print_info(f"Acquired {link_amount} total links. Scraping the links...")

    for link in links:
        print_info(f"Link #{links.index(link)} / {link_amount} : {link}")
        if "pastebin.com/u/" in link:
            user = scrape_user(link)
            if user.dead:
                dead_users.append(user)
            else:
                alive_users.append(user)
        elif "pastebin.com/" in link:
            paste = scrape_paste(link)
            if paste.dead:
                dead_pastes.append(paste)
            else:
                alive_pastes.append(paste)
        else:
            misc_list.append(link)

    pastes_list = [alive_pastes, dead_pastes]
    users_list = [alive_users, dead_users]
    final_result = {'users': users_list, 'pastes': pastes_list, 'misc': misc_list}

    return final_result
